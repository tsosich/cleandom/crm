import React from "react";
import classes from './index.module.sass'
import Form from "./Form";

const Auth = () => {
    return (
        <div className={classes.container}>
            <div className={classes.container_content}>
                <img className={classes.container_content_logo} src='/assets/logo.svg' alt='logo'/>
                <p className={classes.container_content_undertext}>система управления</p>
                <Form/>
            </div>
        </div>
    )
}

export default Auth