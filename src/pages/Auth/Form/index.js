import React from "react";
import classes from './index.module.sass'
import {useFormik} from "formik"
import clsx from "clsx";

const validate = values => {

    const errors = {}

    if(!values.login){
        errors.login = 'обязательное поле'
    }

    if(!values.password){
        errors.password = 'обязательное поле'
    }

    return errors
}

const Form = ({}) => {

    const formik = useFormik({
        initialValues: {
            login: '',
            password: ''
        },
        validate,
        onSubmit: values => {
        },
        initialErrors: {}
    })

    return (
        <form className={classes.form} onSubmit={formik.handleSubmit}>
            <div className={clsx(classes.form_input, formik.errors.login && classes.form_input_error)}>
                <img src='/assets/auth/login.svg' alt='icon'/>
                <input
                    id='login'
                    name='login'
                    type="text"
                    placeholder='логин'
                    onChange={formik.handleChange}
                    value={formik.values.login}
                />
                {formik.errors.login ? <div className={classes.form_input_errortext}>{formik.errors.login}</div> : null}
            </div>
            <div className={clsx(classes.form_input, formik.errors.password && classes.form_input_error)}>
                <img src='/assets/auth/password.svg' alt='icon'/>
                <input
                    id='password'
                    name='password'
                    type="password"
                    placeholder='пароль'
                    onChange={formik.handleChange}
                    value={formik.values.password}
                />
                <img className={classes.form_input_eye} src='/assets/auth/show.svg' alt='eye'/>
                {formik.errors.password ? <div className={classes.form_input_errortext}>{formik.errors.password}</div> : null}
            </div>
            <div className={classes.form_submit}>
                <button type='submit'>ВОЙТИ</button>
            </div>
        </form>
    )
}

export default Form